#include <stdio.h>
#include <stdlib.h>
#include <rand.h>
#include <gb/gb.h>

#include "hacker.c"
#include "MapTiles.c"
#include "map1.c"



// Background
const int bkg_speed = 1;
int bkg_x = 16*6;
int bkg_y = 16*4;
int bkg_width = 32;

int bkg_up = 0;
int bkg_down = 0;
int bkg_right = 0;
int bkg_left = 0;

char modifed_map[1024];

// Hacker The Fox (player)
const int player_speed = 1;
int hacker_x = 16*7;
int hacker_y = 16*4;
const int hacker_width = 16;
const int hacker_height = 16;
int player_animation = 0;

int hacker_up = 0;
int hacker_down = 0;
int hacker_right = 0;
int hacker_left = 0;

int hacker_floorOn = 1;

int i;
int j;
int random_number;

// Floor tiles
// Includes right wall floor tiles
// const char floorTile[9] = {0x11, 0x0D, 0x12, 0x0E, 0x0C, 0x13, 0x0F, 0x14, 0x10};
// Does not include right wall floor tiles. (USE)
// const char floorTile[9] = {0x11, 0x0D, 0x0C, 0x0F, 0x14, 0x10};
// Includes doors.
const char floorTile[19] = {0x0C, 0x10, 0x12, 0x0E, 0x13, 0x14, 0x0F, 0x0C, 0x28, 0x27, 0x29, 0x2A, 0x26, 0x25, 0x2B, 0x2C, 0x0D, 0x11, 0x03};

// Doors, in order from top left to bottom right. 7 in total.
//0 = Open 
//1 = Locked
const char inner_doors_coordinates[7][2] = {{15,5}, {8,10}, {24,10}, {15,15}, {8,20}, {24,20}, {15,25}};
char inner_doors_locked[4][7] = {{0,0,0,0,0,0,0}, {0,0,0,0,0,0,0}, {0,0,0,0,0,0,0}, {0,0,0,0,0,0,0}};

// Stairs, The doors on the outside wall. 10 in total.
// In order from top left to bottom right.
// 0 = Not Visible
// 1 = Going Up
// 2 = Going Down
const char outer_doors_coordinates[6][2] = {{7,0}, {23,0}, {0,15}, {30,15}, {7,30}, {23,30}};
char outer_doors_visible[4][6] = {{0,0,0,0,0,0}, {0,0,0,0,0,0}, {0,0,0,0,0,0}, {0,0,0,0,0,0}};

// Access Points to stairs - are stairs accessible?
const char door_1_2[][] = {{1}, {3,4,2}, {2,5,7,6,3}};
const char door_1_3[][] = {{2}, {1,3,4}, {1,3,5,6,7}};
const char door_1_4[][] = {{1,3}, {4,2}, {6,7,5,2}};
const char door_1_5[][] = {{2,5}, {1,3,6,7}, {1,3,4,5}};
const char door_1_6[][] = {{2,5,7}, {1,3,6}, {2,4,6}};

const char door_2_3[][] = {{1,2}, {3,4}, {3,6,7,5}};
const char door_2_4[][] = {{3}, {1,2,4}, {1,2,5,7,6}};
const char door_2_5[][] = {{1,2,5}, {1,2,4,6,7}, {3,6,7}};
const char door_2_6[][] = {{1,2,5,7}, {1,2,4,6}, {3,6}};

const char door_3_4[][] = {{4}, {2,1,3}, {5,7,6}};
const char door_3_5[][] = {{5}, {4,6,7}, {2,1,3,6,7}};
const char door_3_6[][] = {{4,6}, {2,1,3,6}, {5,7}};

const char door_4_5[][] = {{4,5}, {6,7}, {3,1,2,5}};
const char door_4_6[][] = {{6}, {3,1,2,5,7}, {4,5}};

const char door_5_6[][] = {{7}, {5,2,1,3,6}, {5,4,6}};


// Maps of floors
// char floor_1[1024] = {};
// char floor_2[1024] = {};
// char floor_3[1024] = {};
// char floor_4[1024] = {};

// Hacker's Animations
void update_player_animations(int costume[1]){
	if (costume == 0){
		if (player_animation < 10) {
			// Looking right (NOT moving)
			set_sprite_tile(1, 0); // back ear
			set_sprite_tile(2, 1); // face
			set_sprite_tile(3, 2); // tail
			set_sprite_tile(4, 3); // stomach
		}
		else {
			// Looking right (MOVING)
			set_sprite_tile(1, 16);
			set_sprite_tile(2, 17);
			set_sprite_tile(3, 18);
			set_sprite_tile(4, 19);
		}		
	}
	// Looking left (NOT moving)	
	if (costume == 1){
		if (player_animation < 10) {
			set_sprite_tile(1, 4); // back ear
			set_sprite_tile(2, 5); // face
			set_sprite_tile(3, 6); // tail
			set_sprite_tile(4, 7); // stomach
		}
		else {
			// Moving
			set_sprite_tile(1, 20); // back ear
			set_sprite_tile(2, 21); // face
			set_sprite_tile(3, 22); // tail
			set_sprite_tile(4, 23); // stomach
		}
	}
	// Looking down (NOT moving)	
	if (costume == 2){
		if (player_animation < 10) {
			set_sprite_tile(1, 8); // back ear
			set_sprite_tile(2, 9); // face
			set_sprite_tile(3, 10); // tail
			set_sprite_tile(4, 11); // stomach
		}
		else {
			// Moving
			set_sprite_tile(1, 24); // back ear
			set_sprite_tile(2, 25); // face
			set_sprite_tile(3, 26); // tail
			set_sprite_tile(4, 27); // stomach			
		}
	}
	// Looking up (NOT moving)	
	if (costume == 3){
		if (player_animation < 10) {
			set_sprite_tile(1, 12); // back ear
			set_sprite_tile(2, 13); // face
			set_sprite_tile(3, 14); // tail
			set_sprite_tile(4, 15); // stomach
		}
		else {
			// Moving
			set_sprite_tile(1, 28); // back ear
			set_sprite_tile(2, 29); // face
			set_sprite_tile(3, 30); // tail
			set_sprite_tile(4, 31); // stomach			
		}
	}



}


// Returns 1 if player can move in the direction specified.
int canPlayerMove(int newX, int newY){
	int gridX = (newX - 8 + bkg_x) / 8;
	int gridY = (newY - 8 + bkg_y) / 8;
	int tileIndex = gridX + (gridY * bkg_width);
	char newTile[] = {modifed_map[tileIndex]};


	// Check if door is locked
	for (i=0;i<7;i++){
		if ( gridX == inner_doors_coordinates[i][0] && gridY == inner_doors_coordinates[i][1] && inner_doors_locked[hacker_floorOn-1][i] == 1 ){
			return 0;
		}
	}

	for (i=0;i<19;i++){
		if (newTile[0] == floorTile[i]){
			// The tile the user wants to move to is a floor tile. Allow movement.
			return 1;
		}
	}
	// The tile the user wants to move to is NOT a floor tile. Do NOT allow movement.
	return 0;
}


// Refreshes and draws screen. If given a number acts like a delay
// for controlling Frames Per Second(FPS).
void performantdelay(int numloops){
	for (i=0;i<numloops;i++){
		vsync();
	}
}


// Picks a random number between given min and max.
int randint(int min, int max)
{
	return (DIV_REG % (max - min + 1) + min);
}


// Randomizes the layout of all 4 floors.
void createFloors()
{

	// Choose doors to lock.
	/// 4 Floors
	for (i=0;i<4;i++)
	{
		// 7 inner doors - only 3 doors can be locked per floor
		for (j=0;j<7;j++)
		{
			inner_doors_locked[i][j] = randint(0, 1);	
		}
		//  6 outer doors (Make sure these doors are accessible)
		/// 2 doors need to be chosen per floor. 1 needs to be accessible.
		while ( (outer_doors_visible[i][0] + outer_doors_visible[i][1] + outer_doors_visible[i][2] + outer_doors_visible[i][3] + outer_doors_visible[i][4] + outer_doors_visible[i][5]) != 2 )
		{
			for (j=0;j<6;j++)
			{
				random_number = randint(0, 5);

				// Attempt to spawn stairwell(outer door)
				if ( random_number < 3 )
				{
					// Outer door 1 connections
					if ( j == 1 )
					{
						if ( (inner_doors_locked[i][door_1_3[0][0] - 1] == 0) ||  ((inner_doors_locked[i][door_1_3[1][0] - 1] == 0 && inner_doors_locked[i][door_1_3[1][1] - 1] == 0 && inner_doors_locked[i][door_1_3[1][2] - 1] == 0) || (inner_doors_locked[i][door_1_3[2][0] - 1] == 0 && inner_doors_locked[i][door_1_3[2][1] - 1] == 0 && inner_doors_locked[i][door_1_3[2][2] - 1] == 0 && inner_doors_locked[i][door_1_3[2][3] - 1] == 0 && inner_doors_locked[i][door_1_3[2][4] - 1] == 0))

							// If floor is equal to 1 and center room outer door is not chosen.
							if (i == 0 && j != 3)
							{
								// Spawn outer door 1 going up on floor 1, and spawn outer door going down on floor 2.
								outer_doors_visible[i][j] = 1;
								outer_doors_visible[i+1][j] = 2;
							}
							// else if floor is not 1 or 4 then place outer door going up on chosen floor, and place stairs going down on floor above adjacent spot.
							else if ( i>0 && i != 3 )
							{
								outer_doors_visible[i][j] = 1;
								outer_doors_visible[i+1][j] = 2;
							}
					}
					// Outer door 2 connections
					if ( j == 2 )
					{
						
					}
					else if ( j == 3 )
					{
						continue;
					}
				}

			}
		}
	}

	// Duplicate List
	for (i=0;i<1024;i++)
	{
		modifed_map[i] = Map1[i];
	}

}

void main(){
	waitpad(J_START);

	// Setup sprites and background.	
	set_sprite_data(0, 32, Hacker);
	set_bkg_data(0, 45, MapTiles);

	// Create all 4 floors.
	createFloors();
	
	set_bkg_tiles(0, 0, 32, 32, modifed_map);

	// Spawn HackerTheFox
	move_sprite(1, hacker_x, hacker_y);
	move_sprite(2, hacker_x+8, hacker_y);
	move_sprite(3, hacker_x, hacker_y+8);
	move_sprite(4, hacker_x+8, hacker_y+8);

	SHOW_BKG;
	SHOW_SPRITES;
	DISPLAY_ON;

	// Change map according to choosen layout.

	update_player_animations(2);


	// ### GAME LOOP ###
	while (1){
		// Delay
		performantdelay(1);


		// Toggle player animation 0 till 20.
		player_animation = (player_animation == 20 ? 0 : player_animation+1);

		// Player Movement
		if (joypad() & J_LEFT){
			update_player_animations(1);
			if ( (canPlayerMove(hacker_x - player_speed, hacker_y)) && (canPlayerMove(hacker_x - player_speed, hacker_y + hacker_height/2) ) ) {
				if ( (bkg_x > 0) && (hacker_x < 88) ){
					bkg_x -= bkg_speed;
					bkg_left = 1;
				}
				else{
					hacker_x -= player_speed;
					hacker_left = 1;
				}
			}
		}
		if (joypad() & J_RIGHT){
			update_player_animations(0);
			if ( (canPlayerMove(hacker_x + player_speed + hacker_width, hacker_y)) && (canPlayerMove(hacker_x + player_speed + hacker_width, hacker_y + hacker_height/2))){
				if ( (bkg_x < 96) && (hacker_x > 80) ){
					bkg_x += bkg_speed;
					bkg_right = 1;
				}
				else{
					hacker_x += player_speed;
					hacker_right = 1;
				}
				
			}
		}
		if (joypad() & J_UP){
			update_player_animations(3);
			if ( (canPlayerMove(hacker_x, hacker_y - player_speed)) && (canPlayerMove(hacker_x + hacker_width, hacker_y - player_speed)) ){
				if ( (bkg_y > 0) && (hacker_y < 104) ){
					bkg_y -= bkg_speed;
					bkg_up = 1;
				}
				else {
					hacker_y -= player_speed;
					hacker_up = 1;
				}
			}
		}
		if (joypad() & J_DOWN){
			update_player_animations(2);
			if ( (canPlayerMove(hacker_x, hacker_y + player_speed + (hacker_height/2))) && (canPlayerMove(hacker_x + hacker_width, hacker_y + player_speed + (hacker_height/2)))){
				if ( (bkg_y < 112) && (hacker_y > 56) ){
					bkg_y += bkg_speed;
					bkg_down = 1;
				}
				else {
					hacker_y += player_speed;
					hacker_down = 1;
				}
			}
		}

		if (joypad() & J_A)
		{
			printf("{");
			for (i=0;i<7;i++)
			{
				printf("%d, ", inner_doors_locked[0][i]);
			}
			printf("\b\b}\n");
		}
		
		// Draw HackerTheFox
		move_sprite(1, hacker_x, hacker_y);
		move_sprite(2, hacker_x+8, hacker_y);
		move_sprite(3, hacker_x, hacker_y+8);
		move_sprite(4, hacker_x+8, hacker_y+8);

		// Draw Background
	    move_bkg(bkg_x, bkg_y);	
	}
}
